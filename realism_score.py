import csv

csv_file_name = "blackhair_female_batch.csv"

with open(csv_file_name, mode='w') as csv_file:
    fieldnames = ['image_A_url', 'image_B_url', 'image_C_url', 'image_D_url']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()

    writer.writerow(['John Smith', 'Accounting', 'November'])
    writer.writerow(['Erica Meyers', 'IT', 'March'])